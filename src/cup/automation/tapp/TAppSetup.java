/**
 * 
 */
package cup.automation.tapp;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * @author abernal
 *
 */
public class TAppSetup {
	private DesiredCapabilities capabilities = new DesiredCapabilities();
	TAppServer_Settings settings;
	private String applocation;
	private String apppackage;
	private String version_name;
	private String udid;
	private String appactivity;
	private String iosversion;
	private String iosdevicename;
	private int width;
	private int height;
	private String density;

	public DesiredCapabilities getCapabilities() {
		return this.capabilities;
	}

	public void setCapabilities(DesiredCapabilities capabilities) {
		this.capabilities = capabilities;
	}

	public TAppSetup(TAppServer_Settings settings) {
		this.settings = settings;
	}

	private void setDefaultCapabilities() {
		this.getCapabilities().setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
				UnexpectedAlertBehaviour.IGNORE);
		this.getCapabilities().setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	}

	private void setFirefoxCapabilities() {
		this.setCapabilities(DesiredCapabilities.firefox());
		this.setDefaultCapabilities();
	}

	private void setIECapabilities() {
		this.setCapabilities(DesiredCapabilities.internetExplorer());
		this.setDefaultCapabilities();
		this.getCapabilities().setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
	}

	private void setEdgeCapabilities() {
		this.setCapabilities(DesiredCapabilities.edge());
		this.setDefaultCapabilities();
		this.getCapabilities().setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
	}

	private void setChromeCapabilities() {
		this.setCapabilities(DesiredCapabilities.chrome());
		this.setDefaultCapabilities();
	}

	private void setSafariCapabilities() {
		this.setCapabilities(DesiredCapabilities.safari());
		this.setDefaultCapabilities();
		this.getCapabilities().setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useFirefox()
	 */
	public WebDriver useFirefox() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\geckodriver.exe");
		this.setFirefoxCapabilities();
		this.getCapabilities().setCapability("marionette", true);
		return new FirefoxDriver(this.getCapabilities());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useChrome()
	 */
	public WebDriver useChrome() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\chromedriver.exe");
		this.setChromeCapabilities();
		return new ChromeDriver(this.getCapabilities());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useInterExplorer()
	 */
	public WebDriver useInterExplorer() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.ie.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\IEDriverServer32.exe");
		this.setIECapabilities();
		return new InternetExplorerDriver(this.getCapabilities());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useEdge()
	 */
	public WebDriver useEdge() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.edge.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\MicrosoftWebDriver.exe");
		this.setEdgeCapabilities();
		// System.setProperty("webdriver.edge.driver",
		// "D:/NetBeansProjects/AutomationSuite/MicrosoftWebDriver.exe");
		return new EdgeDriver(this.getCapabilities());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useSafari()
	 */
	public WebDriver useSafari() {
		// TODO Auto-generated method stub
		this.setSafariCapabilities();
		SafariOptions options = new SafariOptions();
		options.setUseCleanSession(true);
		this.getCapabilities().setCapability(SafariOptions.CAPABILITY, options);
		return new SafariDriver(this.getCapabilities());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useAndroid()
	 */
	@SuppressWarnings("rawtypes")
	public AndroidDriver useAndroid() {
		// TODO Auto-generated method stub
		this.setCapabilities(DesiredCapabilities.android());
		this.getCapabilities().setCapability("device", "Android");
		this.getCapabilities().setCapability("noReset", "true");
		this.getCapabilities().setCapability("fullReset", "false");
		this.getCapabilities().setCapability(CapabilityType.BROWSER_NAME, "");
		this.getCapabilities().setCapability("platformVersion", getVersion_name());
		this.getCapabilities().setCapability("platformName", "Android");
		this.getCapabilities().setCapability("deviceName", getUdid());
		if (!getApplocation().equals("")) {
			capabilities.setCapability("app", getApplocation());
		}
		capabilities.setCapability("appPackage", getApppackage());
		capabilities.setCapability("appActivity", getAppactivity());
		try {
			return new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tapp.cup.team.automation.interfaces.TappSetup#useIOS()
	 */
	@SuppressWarnings("rawtypes")
	public IOSDriver useIOS(boolean iphone) {
		if (iphone) {
			this.setCapabilities(DesiredCapabilities.iphone());
		} else {
			this.setCapabilities(DesiredCapabilities.ipad());
		}
		this.getCapabilities().setCapability(CapabilityType.BROWSER_NAME, "iOS");
		this.getCapabilities().setCapability(CapabilityType.VERSION, getIosversion());
		this.getCapabilities().setCapability("platformName", "iOS");
		this.getCapabilities().setCapability("deviceName", getIosdevicename());
		this.getCapabilities().setCapability("app", getApplocation());
		if (!getUdid().equals("")) {
			this.getCapabilities().setCapability("udid", getUdid());
			// capabilities.setCapability("bundleid", pkg);
		}

		// TODO Auto-generated method stub
		try {
			return new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebDriver useRemoteFirefox() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\geckodriver.exe");
		this.setFirefoxCapabilities();
		this.getCapabilities().setCapability("marionette", true);
		try {
			return new RemoteWebDriver(
					new URL("http://" + settings.getFirefox_server() + ":" + settings.getFirefox_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebDriver useRemoteChrome() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\chromedriver.exe");
		this.setChromeCapabilities();
		try {
			return new RemoteWebDriver(
					new URL("http://" + settings.getChrome_server() + ":" + settings.getChrome_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebDriver useRemoteInterExplorer() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.ie.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\IEDriverServer32.exe");
		this.setIECapabilities();
		try {
			return new RemoteWebDriver(
					new URL("http://" + settings.getIe_server() + ":" + settings.getIe_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebDriver useRemoteEdge() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.edge.driver", "\\\\cup-mnl-qa1\\ta_tapp$\\lib\\MicrosoftWebDriver.exe");
		this.setEdgeCapabilities();
		try {
			return new RemoteWebDriver(
					new URL("http://" + settings.getEdge_server() + ":" + settings.getEdge_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public WebDriver useRemoteSafari() {
		// TODO Auto-generated method stub
		this.setSafariCapabilities();
		SafariOptions options = new SafariOptions();
		options.setUseCleanSession(true);
		this.getCapabilities().setCapability(SafariOptions.CAPABILITY, options);
		try {
			return new RemoteWebDriver(
					new URL("http://" + settings.getSafari_server() + ":" + settings.getSafari_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings("rawtypes")
	public AndroidDriver useRemoteAndroid() {
		// TODO Auto-generated method stub
		this.setCapabilities(DesiredCapabilities.android());
		this.getCapabilities().setCapability("device", "Android");
		this.getCapabilities().setCapability("noReset", "true");
		this.getCapabilities().setCapability("fullReset", "false");
		this.getCapabilities().setCapability(CapabilityType.BROWSER_NAME, "");
		this.getCapabilities().setCapability("platformVersion", getVersion_name());
		this.getCapabilities().setCapability("platformName", "Android");
		this.getCapabilities().setCapability("deviceName", getUdid());
		if (!getApplocation().equals("")) {
			capabilities.setCapability("app", getApplocation());
		}
		capabilities.setCapability("appPackage", getApppackage());
		capabilities.setCapability("appActivity", getAppactivity());
		return connectToGrid(this.getCapabilities(), getVersion_name(), getWidth(), getHeight(), getDensity(),
				getUdid());

	}

	@SuppressWarnings("rawtypes")
	public IOSDriver useRemoteIOS(boolean iphone) {

		if (iphone) {
			this.setCapabilities(DesiredCapabilities.iphone());
		} else {
			this.setCapabilities(DesiredCapabilities.ipad());
		}
		this.getCapabilities().setCapability(CapabilityType.BROWSER_NAME, "iOS");
		this.getCapabilities().setCapability(CapabilityType.VERSION, getIosversion());
		this.getCapabilities().setCapability("platformName", "iOS");
		this.getCapabilities().setCapability("deviceName", getIosdevicename());
		this.getCapabilities().setCapability("app", getApplocation());
		if (!getUdid().equals("")) {
			this.getCapabilities().setCapability("udid", getUdid());
			// capabilities.setCapability("bundleid", pkg);
		}
		// TODO Auto-generated method stub
		try {
			return new IOSDriver(
					new URL("http://" + settings.getIos_server() + ":" + settings.getIos_port() + "/wd/hub"),
					this.getCapabilities());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public AndroidDriver connectToGrid(DesiredCapabilities capabilities, String version, int width, int height,
			String density, String devicename) {
		TAppRemoteSettings constants = new TAppRemoteSettings();
		try {
			URL url = null;
			HttpURLConnection conn = null;

			url = new URL("http://" + constants.getANDROID_IP()
					+ "/remoteserver/drivers/controller.php?opt=start_node_server&port=" + constants.getANDROID_PORT());

			TAppSocketClient client = new TAppSocketClient(constants.getANDROID_IP(), 9898);
			client.connectToServer();
			// UI.updateTerminal("Connecting to port " + port + "...");
			if (client.getResponse().equals("Connected")) {
				System.out.println("Connected");
				if (!getUdid().equals("")) {
					client.sendRequest("create_emulator " + version + " " + width + " " + height + " "
							+ constants.getANDROID_PORT() + " " + density);
					capabilities.setCapability("avd", "emulator" + constants.getANDROID_PORT());
					Thread.sleep(15000);
					return new AndroidDriver(new URL(
							"http://" + constants.getANDROID_IP() + ":" + constants.getANDROID_PORT() + "/wd/hub"),
							capabilities);
				} else {
					Thread.sleep(5000);
					client.sendRequest("open_port " + constants.getANDROID_PORT() + " " + getUdid() + " " + version);
					Thread.sleep(15000);
					return new AndroidDriver(new URL(
							"http://" + constants.getANDROID_IP() + ":" + constants.getANDROID_PORT() + "/wd/hub"),
							capabilities);
				}
				// settingsdata[14] = port;
			} else {
				// UI.updateTerminal("Port is in use or device cannot start..");
				constants.setANDROID_PORT(constants.getANDROID_PORT() + 2);
				connectToGrid(capabilities, version, width, height, density, devicename);
			}

		} catch (Exception ex) {
			//// Logger.getLogger(TestAppium9_8stable.class.getName()).log(Level.SEVERE,
			//// null, ex);
			// UI.updateTerminal("Port is in use");
			connectToGrid(capabilities, version, width, height, density, devicename);
		}
		// Log("http://"+ip+":"+port+"/wd/hub");
		return null;

		// Log("Driver:" + driver);

	}

	/**
	 * @return the applocation
	 */
	public String getApplocation() {
		return applocation;
	}

	/**
	 * @param applocation
	 *            the applocation to set
	 */
	public void setApplocation(String applocation) {
		this.applocation = applocation;
	}

	/**
	 * @return the apppackage
	 */
	public String getApppackage() {
		return apppackage;
	}

	/**
	 * @param apppackage
	 *            the apppackage to set
	 */
	public void setApppackage(String apppackage) {
		this.apppackage = apppackage;
	}

	/**
	 * @return the version_name
	 */
	String getVersion_name() {
		return version_name;
	}

	/**
	 * @param version_name
	 *            the version_name to set
	 */
	void setVersion_name(String version_name) {
		this.version_name = version_name;
	}

	/**
	 * @return the udid
	 */
	public String getUdid() {
		return udid;
	}

	/**
	 * @param udid
	 *            the udid to set
	 */
	public void setUdid(String udid) {
		this.udid = udid;
	}

	/**
	 * @return the appactivity
	 */
	public String getAppactivity() {
		return appactivity;
	}

	/**
	 * @param appactivity
	 *            the appactivity to set
	 */
	public void setAppactivity(String appactivity) {
		this.appactivity = appactivity;
	}

	/**
	 * @return the iosversion
	 */
	public String getIosversion() {
		return iosversion;
	}

	/**
	 * @param iosversion
	 *            the iosversion to set
	 */
	public void setIosversion(String iosversion) {
		this.iosversion = iosversion;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the density
	 */
	public String getDensity() {
		return density;
	}

	/**
	 * @param density
	 *            the density to set
	 */
	public void setDensity(String density) {
		this.density = density;
	}

	/**
	 * @return the iosdevicename
	 */
	public String getIosdevicename() {
		return iosdevicename;
	}

	/**
	 * @param iosdevicename
	 *            the iosdevicename to set
	 */
	public void setIosdevicename(String iosdevicename) {
		this.iosdevicename = iosdevicename;
	}
	
	public void startTest(){
		
	}
}
