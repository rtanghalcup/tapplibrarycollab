package cup.automation.tapp;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.ScreenOrientation;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.Connection;

public class TAppAndroidTest {

	static TAppUtilities utilities = new TAppUtilities();
	static String paramValue;
	static Map<String, String> valuesMap;

	public static final Object Click(AndroidElement element) {
		try {
			element.click();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public static final Object From(AndroidDriver driver, AndroidElement element, String parameter, int operation_id) {
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, parameter, "parameter");
		} catch (NullPointerException e) {

		}
		switch (operation_id) {
		case TappOperations.TYPE_TEXT:
			element.click();
			try {
				element.clear();
			} catch (Exception e) {

			}
			try {
				element.setValue(paramValue);
				driver.navigate().back();
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;

		case TappOperations.SELECT_ITEM:
			try {
				element.click();

				if (!parameter.toLowerCase().startsWith("**get**")) {
					AndroidElement element2 = (AndroidElement) driver
							.findElement(By.name(parameter));
					element2.click();
				}

				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.SEARCH_FOR:
			try {
				element.click();
				element.sendKeys(Keys.chord(Keys.CONTROL, "a"), "");
				element.sendKeys(paramValue);
				element.sendKeys(Keys.ENTER);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object Navigate_Backwards(AndroidDriver driver) {
		try {
			driver.navigate().back();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object Validate(AndroidDriver androiddriver, List<AndroidElement> androidelements,
			AndroidElement androidelement, String validation_data, int validation_id, AndroidElement compare) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}

		String texttotest = "";

		texttotest = androidelement.getText();

		if (texttotest.equals("")) {
			texttotest = androidelement.getAttribute("value");
		}

		double valuetotest;

		try {
			valuetotest = Double.parseDouble(androidelement.getText());
		} catch (NullPointerException | NumberFormatException e) {
			valuetotest = 0;
		}
		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (texttotest.equals(paramValue)) {
//				System.out.println(androidelement.get + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (texttotest.equalsIgnoreCase(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!texttotest.equals(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (texttotest.contains(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return texttotest;
			}

			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (texttotest.startsWith(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " starts with " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not start with " + paramValue);
				return texttotest;
			}

			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (texttotest.toLowerCase().startsWith(paramValue.toLowerCase())) {
//				System.out.println(step.getObject().getObject_identifier() + " starts with " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not start with " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!texttotest.contains(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!texttotest.toLowerCase().contains(paramValue.toLowerCase())) {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_GREATER_THAN:
			if (valuetotest > Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is greater than " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not greater than " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.INTEGER_GREATER_THAN_OR_EQUAL_TO:
			if (valuetotest >= Double.parseDouble(paramValue)) {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is greater than or equal to " + paramValue);
				return true;
			} else {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is not greater than or equal to " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.INTEGER_LESS_THAN:
			if (valuetotest < Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is less than " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not less than " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.INTEGER_LESS_THAN_OR_EQUAL_TO:
			if (valuetotest <= Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is less than or equal to " + paramValue);
				return true;
			} else {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is not less than or equal to " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.INTEGER_EQUAL_TO:
			if (valuetotest == Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.INTEGER_NOT_EQUAL_TO:
			if (valuetotest != Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return valuetotest;
			}
			// break;
		case TappValidations.IN_ASCENDING_ORDER:
			String[] elementvalues = new String[androidelements.size()];

			for (int x = 0; x < androidelements.size(); x++) {
				elementvalues[x] = androidelements.get(x).getText();
			}

			Arrays.sort(elementvalues);
			int stringequal = 0;
			for (int ctr_elems = 0; ctr_elems < androidelements.size(); ctr_elems++) {
				if (androidelements.get(ctr_elems).getText().compareTo(elementvalues[ctr_elems]) == 0) {
					stringequal++;
				}
			}
			if (stringequal == androidelements.size()) {
				System.out.println("List is in order");
				return true;

			} else {
				String actual = "";
				String ordered = "";

				for (int ctr_elems = 0; ctr_elems < androidelements.size(); ctr_elems++) {
					actual += androidelements.get(ctr_elems).getText() + ", ";
					ordered += elementvalues[ctr_elems] + ", ";
				}
				System.out.println("Actual order is " + actual);
				System.out.println("Ordered list is " + ordered);
				return actual;
			}
			// break;
		case TappValidations.IN_DESCENDING_ORDER:
			elementvalues = new String[androidelements.size()];

			for (int x = 0; x < androidelements.size(); x++) {
				elementvalues[x] = androidelements.get(x).getText();
			}

			Arrays.sort(elementvalues);
			stringequal = 0;
			for (int ctr_elems = 0; ctr_elems < androidelements.size(); ctr_elems++) {
				if (androidelements.get(ctr_elems).getText()
						.compareTo(elementvalues[(androidelements.size() - 1) - ctr_elems]) == 0) {
					stringequal++;
				}
			}

			if (stringequal == androidelements.size()) {
				System.out.println("List is in order");
				return true;

			} else {
				String actual = "";
				String ordered = "";

				for (int ctr_elems = 0; ctr_elems < androidelements.size(); ctr_elems++) {
					actual += androidelements.get(ctr_elems).getText() + ", ";
					ordered += elementvalues[(androidelements.size() - 1) - ctr_elems] + ", ";
				}
				System.out.println("Actual order is " + actual);
				System.out.println("Ordered list is " + ordered);
				return actual;
			}
			// break;
		case TappValidations.EXISTS:
			if (androidelements.size() > 0) {
//				System.out.println(step.getObject().getObject_identifier() + " exists");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " do not exist");
				return false;
			}
			// break;
		case TappValidations.NOT_EXISTS:
			if (androidelements.size() == 0) {
//				System.out.println(step.getObject().getObject_identifier() + " do not exist");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " exists");
				return false;
			}
			// break;
		case TappValidations.IS_SELECTED:
			if (androidelement.isSelected()) {
//				System.out.println(step.getObject().getObject_identifier() + " is selected");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not selected");
				return false;
			}
			// break;
		case TappValidations.IS_NOT_SELECTED:
			if (!androidelement.isSelected()) {
//				System.out.println(step.getObject().getObject_identifier() + " is not selected");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is selected");
				return false;
			}
			// break;
		case TappValidations.IN_PORTRAIT:
			if (androiddriver.getOrientation() == ScreenOrientation.PORTRAIT) {
				System.out.println("Screen is in portrait");
				return true;
			} else {
				System.out.println("Screen is not in portrait");
				return false;
			}
			// break;
		case TappValidations.IN_LANDSCAPE:
			if (androiddriver.getOrientation() == ScreenOrientation.LANDSCAPE) {
				System.out.println("Screen is in landscape");
				return true;
			} else {
				System.out.println("Screen is not in landscape");
				return false;
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object ValidateTitle(AndroidDriver androiddriver, String validation_data, int validation_id) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}

		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (androiddriver.getTitle().equals(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (androiddriver.getTitle().equalsIgnoreCase(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!androiddriver.getTitle().equals(paramValue)) {
				System.out.println("Title is not equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is equal to " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (androiddriver.getTitle().contains(paramValue)) {
				System.out.println("Title contains " + paramValue);
				return true;
			} else {
				System.out.println("Title does not contain " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (androiddriver.getTitle().startsWith(paramValue)) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (androiddriver.getTitle().toLowerCase().startsWith(paramValue.toLowerCase())) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!androiddriver.getTitle().contains(paramValue)) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return androiddriver.getTitle();
			}

			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!androiddriver.getTitle().toLowerCase().contains(paramValue.toLowerCase())) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return androiddriver.getTitle();
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object ValidateURL(AndroidDriver androiddriver, String validation_data, int validation_id) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}
		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (androiddriver.getCurrentUrl().equals(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (androiddriver.getCurrentUrl().equalsIgnoreCase(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!androiddriver.getCurrentUrl().equals(paramValue)) {
				System.out.println("Title is not equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is equal to " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (androiddriver.getCurrentUrl().contains(paramValue)) {
				System.out.println("Title contains " + paramValue);
				return true;
			} else {
				System.out.println("Title does not contain " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (androiddriver.getCurrentUrl().startsWith(paramValue)) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (androiddriver.getCurrentUrl().toLowerCase().startsWith(paramValue.toLowerCase())) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!androiddriver.getCurrentUrl().contains(paramValue)) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return androiddriver.getCurrentUrl();
			}

			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!androiddriver.getCurrentUrl().toLowerCase().contains(paramValue.toLowerCase())) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return androiddriver.getCurrentUrl();
			}
			// break;
		default:
			return false;
		}

	}

	public static final Object Switch(AndroidElement androidelement) {
		// TODO Auto-generated method stub
		try {
			Click(androidelement);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToWebView(AndroidDriver androiddriver) {
		// TODO Auto-generated method stub
		try {
			androiddriver.switchTo().window("WEBVIEW");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToWebViewWithURL(AndroidDriver androiddriver, String object) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
		} catch (NullPointerException e) {

		}

		try {
			androiddriver.get(paramValue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToNative(AndroidDriver androiddriver) {
		// TODO Auto-generated method stub
		try {
			androiddriver.switchTo().window("NATIVE_APP");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SubmitForm(AndroidElement androidelement) {
		// TODO Auto-generated method stub
		try {
			androidelement.submit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToIFrame(AndroidDriver androiddriver, String object) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
		} catch (NullPointerException e) {

		}

		try {
			androiddriver.switchTo().frame(paramValue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToDefaultWebContent(AndroidDriver androiddriver) {
		// TODO Auto-generated method stub
//		try {
//			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
//		} catch (NullPointerException e) {
//
//		}

		try {
			androiddriver.switchTo().defaultContent();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object Swipe(AndroidDriver androiddriver, String object, int operation_id) {
		// TODO Auto-generated method stub
		String[] points = null;
		double right = 0;
		double left = 0;
		double vertical = 0;

		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
		} catch (NullPointerException e) {

		}
		switch (operation_id) {
		case TappOperations.TOP_TO_BOTTOM:
			double top;
			double bottom;
			double horizontal;
			try {
				points = paramValue.trim().split(" ");
				top = Double.parseDouble(points[0].trim()) / 100;
				bottom = Double.parseDouble(points[1].trim()) / 100;
				horizontal = 0;
				if (points.length == 3) {
					horizontal = Double.parseDouble(points[2].trim()) / 100;
				} else {
					horizontal = 50;
				}

				androiddriver.swipe((int) (androiddriver.manage().window().getSize().width * horizontal),
						(int) (androiddriver.manage().window().getSize().height * top),
						(int) (androiddriver.manage().window().getSize().width * horizontal),
						(int) (androiddriver.manage().window().getSize().height * bottom), 500);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.BOTTOM_TO_TOP:
			try {
				points = paramValue.trim().split(" ");
				bottom = (100 - Double.parseDouble(points[0].trim())) / 100;
				top = (100 - Double.parseDouble(points[1].trim())) / 100;
				if (points.length == 3) {
					horizontal = Double.parseDouble(points[2].trim()) / 100;
				} else {
					horizontal = 50;
				}
				androiddriver.swipe((int) (androiddriver.manage().window().getSize().width * horizontal),
						(int) (androiddriver.manage().window().getSize().height * bottom),
						(int) (androiddriver.manage().window().getSize().width * horizontal),
						(int) (androiddriver.manage().window().getSize().height * top), 500);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.RIGHT_TO_LEFT:
			try {
				points = paramValue.trim().split(" ");
				right = (100 - Double.parseDouble(points[0].trim())) / 100;
				left = (100 - Double.parseDouble(points[1].trim())) / 100;

				if (points.length == 3) {
					vertical = Double.parseDouble(points[2].trim()) / 100;
				} else {
					vertical = 50;
				}

				androiddriver.swipe((int) (androiddriver.manage().window().getSize().width * right),
						(int) (androiddriver.manage().window().getSize().height * vertical),
						(int) (androiddriver.manage().window().getSize().width * left),
						(int) (androiddriver.manage().window().getSize().height * vertical), 500);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.LEFT_TO_RIGHT:
			try {
				points = paramValue.split(" ");
				left = Double.parseDouble(points[0].trim()) / 100;
				right = Double.parseDouble(points[1].trim()) / 100;
				if (points.length == 3) {
					vertical = Double.parseDouble(points[2].trim()) / 100;
				} else {
					vertical = 50;
				}
				androiddriver.swipe((int) (androiddriver.manage().window().getSize().width * left),
						(int) (androiddriver.manage().window().getSize().height * vertical),
						(int) (androiddriver.manage().window().getSize().width * right),
						(int) (androiddriver.manage().window().getSize().height * vertical), 500);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object Wait(int seconds) {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(seconds);
			return true;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public static final Object CloseMobileApp(AndroidDriver androiddriver) {
		// TODO Auto-generated method stub
		try {
			androiddriver.closeApp();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object HideMobileApp(AndroidDriver androiddriver, int seconds) {
		// TODO Auto-generated method stub
		try {
			androiddriver.runAppInBackground(seconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object LaunchMobileApp(AndroidDriver androiddriver) {
		// TODO Auto-generated method stub
		try {
			androiddriver.launchApp();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object TurnConnection(AndroidDriver androiddriver, String connection_type, int operation_id) {
		// TODO Auto-generated method stub
		if (connection_type.equalsIgnoreCase("wifi")) {
			switch (operation_id) {
			case TappOperations.TO_ON:
				// NetworkConnection network;
				// network.setNetworkConnection(ConnectionType.WIFI);
				try {
					androiddriver.setConnection(Connection.WIFI);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			case TappOperations.TO_OFF:
				try {
					androiddriver.setConnection(Connection.NONE);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			default:
				return false;
			}
		} else if (connection_type.equalsIgnoreCase("data")) {
			switch (operation_id) {
			case TappOperations.TO_ON:
				// NetworkConnection network;
				// network.setNetworkConnection(ConnectionType.WIFI);
				try {
					androiddriver.setConnection(Connection.DATA);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			case TappOperations.TO_OFF:
				try {
					androiddriver.setConnection(Connection.NONE);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			default:
				return false;
			}
		} else if (connection_type.equalsIgnoreCase("airplane")) {
			switch (operation_id) {
			case TappOperations.TO_ON:
				// NetworkConnection network;
				// network.setNetworkConnection(ConnectionType.WIFI);
				try {
					androiddriver.setConnection(Connection.AIRPLANE);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			case TappOperations.TO_OFF:
				try {
					androiddriver.setConnection(Connection.NONE);
					return true;
				} catch (Exception e) {
					return false;
				}
				// break;
			default:
				return false;
			}
		} else {
			return false;
		}
	}

	public static final Object RotateOrientation(AndroidDriver androiddriver, int operation_id) {
		// TODO Auto-generated method stub
		switch (operation_id) {
		case TappOperations.TO_PORTRAIT:
			try {
				androiddriver.rotate(ScreenOrientation.PORTRAIT);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.TO_LANDSCAPE:
			try {
				androiddriver.rotate(ScreenOrientation.LANDSCAPE);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		default:
			return false;
		}
	}
}
