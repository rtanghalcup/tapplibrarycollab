package cup.automation.tapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

public class TAppUtilities {
	public Object getFunctionResult(Map<String, String> valuesMap, String result_reference, String asobject) {
		Object resValue = "";
		String index = "";
		switch (asobject.toLowerCase().trim()) {
		case "object":
//			index = step.getObject().getObject_identifier();
			index = result_reference;
			break;
		case "parameter":
//			index = step.getOper_params().getOperation_params();
			index = result_reference;
			break;
		case "data":
//			index = step.getData().getData_params();
			index = result_reference;
			break;
		default:
		}
		String tappfunction = index.trim().replace("\"", "").substring(0,
				index.trim().replace("\"", "").lastIndexOf("*") + 1);
		switch (tappfunction.toLowerCase()) {
		case "**map**":
			resValue = getMapValue(valuesMap, index.trim().replace("\"", "").replace("**map**", "").trim());
			break;
		case "**datetime**":
			String dt_format = index.trim().replace("\"", "").replace("**datetime**", "").trim();
			Calendar fnow = Calendar.getInstance();
			DateFormat dateFormat;
			switch (dt_format.toLowerCase()) {
			case "longdate":
				dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
				resValue = dateFormat.format(fnow.getTime());
				break;
			case "shortdate":
				dateFormat = new SimpleDateFormat("MM/dd/yy");
				resValue = dateFormat.format(fnow.getTime());
				break;
			default:
				dateFormat = new SimpleDateFormat(dt_format);
				resValue = dateFormat.format(fnow.getTime());
			}
			break;
		case "**colrow**":
			String[] strcolrow = index.trim().replace("\"", "").replace("**colrow**", "").trim().split(",");
			resValue = parseStrColRow(strcolrow);
			// return
			break;
		case "**tag**":
			String xmltag = index.trim().replace("\"", "").replace("**tag**", "").trim();
			resValue = xmltag;
			// return
			break;
		default:
			resValue = index.trim().replace("\"", "");
		}

		return resValue;
	}

	private Object parseStrColRow(String[] strcolrow) {
		Integer[] colrow = new Integer[strcolrow.length];
		colrow[0] = Integer.parseInt(strcolrow[0].trim());
		colrow[1] = Integer.parseInt(strcolrow[1].trim());
		return colrow;
	}

	public static String getMapValue(Map<String, String> valuesMap, String key) {
		String entryValue = "";
		for (Map.Entry<String, String> entry : valuesMap.entrySet()) {
			if (entry.getKey().equals(key.replaceAll("[^a-zA-Z]", ""))) {
				entryValue = entry.getValue();
			}
		}
		return entryValue;
	}
}
