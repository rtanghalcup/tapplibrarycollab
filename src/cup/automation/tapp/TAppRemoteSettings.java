package cup.automation.tapp;

public class TAppRemoteSettings {
String WEB_IP = "tapp2web-dev.internal";
int WEB_PORT = 5555;
String ANDROID_IP = "tapp2web-dev.internal";
int ANDROID_PORT = 5555;
String IOS_IP = "tapp2web-dev.internal";
int IOS_PORT = 5555;
/**
 * @return the wEB_IP
 */
public String getWEB_IP() {
	return WEB_IP;
}
/**
 * @param wEB_IP the wEB_IP to set
 */
public void setWEB_IP(String wEB_IP) {
	WEB_IP = wEB_IP;
}
/**
 * @return the wEB_PORT
 */
public int getWEB_PORT() {
	return WEB_PORT;
}
/**
 * @param wEB_PORT the wEB_PORT to set
 */
public void setWEB_PORT(int wEB_PORT) {
	WEB_PORT = wEB_PORT;
}
/**
 * @return the aNDROID_IP
 */
public String getANDROID_IP() {
	return ANDROID_IP;
}
/**
 * @param aNDROID_IP the aNDROID_IP to set
 */
public void setANDROID_IP(String aNDROID_IP) {
	ANDROID_IP = aNDROID_IP;
}
/**
 * @return the aNDROID_PORT
 */
public int getANDROID_PORT() {
	return ANDROID_PORT;
}
/**
 * @param aNDROID_PORT the aNDROID_PORT to set
 */
public void setANDROID_PORT(int aNDROID_PORT) {
	ANDROID_PORT = aNDROID_PORT;
}
/**
 * @return the iOS_IP
 */
public String getIOS_IP() {
	return IOS_IP;
}
/**
 * @param iOS_IP the iOS_IP to set
 */
public void setIOS_IP(String iOS_IP) {
	IOS_IP = iOS_IP;
}
/**
 * @return the iOS_PORT
 */
public int getIOS_PORT() {
	return IOS_PORT;
}
/**
 * @param iOS_PORT the iOS_PORT to set
 */
public void setIOS_PORT(int iOS_PORT) {
	IOS_PORT = iOS_PORT;
}

}
