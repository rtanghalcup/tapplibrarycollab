package cup.automation.tapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class TAppWebTest {

	static TAppUtilities utilities = new TAppUtilities();
	static String paramValue;
	static Map<String, String> valuesMap;
	private static boolean isWithinTable;
	private static List<WebElement> rows_table = new ArrayList<WebElement>();
	private static List<WebElement> columns_table = new ArrayList<WebElement>();
	private static boolean isFound;
	private static int selectedrow;

	public static final Object Click(WebElement element) {
		try {
			element.click();
			System.out.println("Successfully clicked: " + element.getTagName());
			return true;
		} catch (Exception e) {
			return "Clicking " + element.getTagName() + " failed.";
		}
	}

	public static final Object From(WebElement element, String parameter, int operation_id) {
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, parameter, "parameter");
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		switch (operation_id) {
		case TappOperations.TYPE_TEXT:
			try {
				element.click();
				element.clear();
				element.sendKeys(paramValue);
				System.out.println("Successfully typed " + paramValue + " to " + element.getTagName());
				return true;
			} catch (Exception e) {
				System.out.println("Typing " + paramValue + " failed.");
			}
			// break;
			return false;
		case TappOperations.SELECT_ITEM:
			try {
				if (element.getTagName().equalsIgnoreCase("select")) {
					Select select = new Select(element);
					try {
						select.selectByValue(parameter);
						return true;
					} catch (Exception e) {
						select.selectByVisibleText(parameter);
						return true;
					}
					// try {
					// element.click();
					// element.sendKeys(Keys.TAB);
					// return true;
					// } catch (Exception e) {
					// return false;
					// }
				} else {
					element.click();
					// widget2=identifyWidget(driver,refValue);
					try {
						element.sendKeys(paramValue);
						element.sendKeys(Keys.TAB);
						return true;
					} catch (Exception e) {
						element.click();
						return true;
					}
				}
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.SEARCH_FOR:
			try {
				element.click();
				element.clear();
				// element.sendKeys(Keys.chord(Keys.CONTROL, "a"), "");
				element.sendKeys(paramValue);
				element.sendKeys(Keys.ENTER);
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.STORE_VALUE_TO_MAP:
			try {
				String mapvar = "", valuetomap = "";
				mapvar = paramValue;
				valuetomap = element.getText();

				if (valuetomap.equals("")) {
					valuetomap = element.getAttribute("value");
				}

				valuesMap.put(mapvar.replaceAll("[^a-zA-Z]", ""), valuetomap);

				return true;
			} catch (Exception e) {
				return false;
			}
			// break;

		}
		return false;
	}

	public static final Object Navigate_Backwards(WebDriver driver) {
		try {
			driver.navigate().back();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object Validate(boolean isFile, String filename, WebDriver driver, List<WebElement> webelements,
			WebElement webelement, String parameter, String validation_data, int validation_id, WebElement compare) {
		// TODO Auto-generated method stub

		String temptexttotest = "";
		String texttotest = "";

		if (webelement != null) {
			try {
				temptexttotest = compare.getText();
			} catch (NullPointerException e) {

			}

			if (temptexttotest.equals("")) {
				try {
					temptexttotest = compare.getAttribute("value");
				} catch (NullPointerException e) {

				}
			}

			texttotest = webelement.getText();

			if (texttotest.equals("")) {
				texttotest = webelement.getAttribute("value");
			}
		}

		if (compare == null) {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} else {
			paramValue = temptexttotest;
		}

		if (isFile) {
			FileValidation fo = new FileValidation(filename);
			System.out.println("isExcel: " + fo.isExcel());
			if (parameter.startsWith("**colrow**")) {
				Integer[] colrow = (Integer[]) utilities.getFunctionResult(valuesMap, parameter, "parameter");
				System.out.println("Col: " + colrow[0] + " Row: " + colrow[1]);
				if (fo.isExcel()) {
					texttotest = fo.displayExcelCellContent(colrow[0], colrow[1]);
					System.out.println("Content: " + texttotest);
				} else {
					texttotest = fo.displayGenericCellContent(colrow[0], colrow[1]);
					System.out.println("Content: " + texttotest);
				}
			} else if (parameter.startsWith("**tag**")) {
				String tag = (String) utilities.getFunctionResult(valuesMap, parameter, "parameter");
				texttotest = fo.displayXMLNodeContent(tag);
				System.out.println("Content: " + texttotest);
			} else {
				if (fo.isExcel()) {
					texttotest = fo.displayExcelContent();
					System.out.println("Content: " + texttotest);
				} else if (fo.isXML()) {
					texttotest = fo.displayXMLContent();
					System.out.println("Content: " + texttotest);
				} else {
					texttotest = fo.displayGenericContent();
					System.out.println("Content: " + texttotest);
				}
			}
		}

		double valuetotest = 0;

		if (webelement != null) {
			try {
				valuetotest = Double.parseDouble(webelement.getText());
			} catch (NullPointerException | NumberFormatException e) {
				valuetotest = 0;
			}
		}
		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (texttotest.equals(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (texttotest.equalsIgnoreCase(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!texttotest.equals(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (texttotest.contains(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return texttotest;
			}

			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (texttotest.startsWith(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " starts with " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not start with " + paramValue);
				return texttotest;
			}

			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (texttotest.toLowerCase().startsWith(paramValue.toLowerCase())) {
//				System.out.println(step.getObject().getObject_identifier() + " starts with " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " does not start with " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!texttotest.contains(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!texttotest.toLowerCase().contains(paramValue.toLowerCase())) {
//				System.out.println(step.getObject().getObject_identifier() + " does not contain " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " contains " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_GREATER_THAN:
			if (valuetotest > Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is greater than " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not greater than " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_GREATER_THAN_OR_EQUAL_TO:
			if (valuetotest >= Double.parseDouble(paramValue)) {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is greater than or equal to " + paramValue);
				return true;
			} else {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is not greater than or equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_LESS_THAN:
			if (valuetotest < Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is less than " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not less than " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_LESS_THAN_OR_EQUAL_TO:
			if (valuetotest <= Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is less than or equal to " + paramValue);
				return true;
			} else {
//				System.out.println(
//						step.getObject().getObject_identifier() + " is not less than or equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_EQUAL_TO:
			if (valuetotest == Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.INTEGER_NOT_EQUAL_TO:
			if (valuetotest != Double.parseDouble(paramValue)) {
//				System.out.println(step.getObject().getObject_identifier() + " is not equal to " + paramValue);
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is equal to " + paramValue);
				return texttotest;
			}
			// break;
		case TappValidations.IN_ASCENDING_ORDER:
			String[] elementvalues = new String[webelements.size()];

			for (int x = 0; x < webelements.size(); x++) {
				elementvalues[x] = webelements.get(x).getText();
			}

			Arrays.sort(elementvalues);
			int stringequal = 0;
			for (int ctr_elems = 0; ctr_elems < webelements.size(); ctr_elems++) {
				if (webelements.get(ctr_elems).getText().compareTo(elementvalues[ctr_elems]) == 0) {
					stringequal++;
				}
			}
			if (stringequal == webelements.size()) {
				System.out.println("List is in order");
				return true;
			} else {
				String actual = "";
				String ordered = "";

				for (int ctr_elems = 0; ctr_elems < webelements.size(); ctr_elems++) {
					actual += webelements.get(ctr_elems).getText() + ", ";
					ordered += elementvalues[ctr_elems] + ", ";
				}
				System.out.println("Actual order is " + actual);
				System.out.println("Ordered list is " + ordered);
				return actual;
			}
			// break;
		case TappValidations.IN_DESCENDING_ORDER:
			elementvalues = new String[webelements.size()];

			for (int x = 0; x < webelements.size(); x++) {
				elementvalues[x] = webelements.get(x).getText();
			}

			Arrays.sort(elementvalues);
			stringequal = 0;
			for (int ctr_elems = 0; ctr_elems < webelements.size(); ctr_elems++) {
				if (webelements.get(ctr_elems).getText()
						.compareTo(elementvalues[(webelements.size() - 1) - ctr_elems]) == 0) {
					stringequal++;
				}
			}

			if (stringequal == webelements.size()) {
				System.out.println("List is in order");
				return true;
			} else {
				String actual = "";
				String ordered = "";

				for (int ctr_elems = 0; ctr_elems < webelements.size(); ctr_elems++) {
					actual += webelements.get(ctr_elems).getText() + ", ";
					ordered += elementvalues[(webelements.size() - 1) - ctr_elems] + ", ";
				}
				System.out.println("Actual order is " + actual);
				System.out.println("Ordered list is " + ordered);
				return actual;
			}
			// break;
		case TappValidations.EXISTS:
			if (webelements.size() > 0) {
//				System.out.println(step.getObject().getObject_identifier() + " exists");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " do not exist");
				return false;
			}
			// break;
		case TappValidations.NOT_EXISTS:
			if (webelements.size() == 0) {
//				System.out.println(step.getObject().getObject_identifier() + " do not exist");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " exists");
				return false;
			}
			// break;
		case TappValidations.IS_SELECTED:
			if (webelement.isSelected()) {
//				System.out.println(step.getObject().getObject_identifier() + " is selected");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is not selected");
				return false;
			}
			// break;
		case TappValidations.IS_NOT_SELECTED:
			if (!webelement.isSelected()) {
//				System.out.println(step.getObject().getObject_identifier() + " is not selected");
				return true;
			} else {
//				System.out.println(step.getObject().getObject_identifier() + " is selected");
				return false;
			}
			// break;
		}
		return false;
	}

	public static final Object ValidateTitle(WebDriver driver, String validation_data, int validation_id) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}

		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (driver.getTitle().equals(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (driver.getTitle().equalsIgnoreCase(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!driver.getTitle().equals(paramValue)) {
				System.out.println("Title is not equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is equal to " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (driver.getTitle().contains(paramValue)) {
				System.out.println("Title contains " + paramValue);
				return true;
			} else {
				System.out.println("Title does not contain " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (driver.getTitle().startsWith(paramValue)) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (driver.getTitle().toLowerCase().startsWith(paramValue.toLowerCase())) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return driver.getTitle();
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!driver.getTitle().contains(paramValue)) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return driver.getTitle();
			}

			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!driver.getTitle().toLowerCase().contains(paramValue.toLowerCase())) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return driver.getTitle();
			}
			// break;
		default:
			return false;
		}

	}

	public static final Object ValidateURL(WebDriver driver, String validation_data, int validation_id) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}
		switch (validation_id) {
		case TappValidations.STRING_EQUALS:
			if (driver.getCurrentUrl().equals(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_EQUALS_IGNORE_CASE:
			if (driver.getCurrentUrl().equalsIgnoreCase(paramValue)) {
				System.out.println("Title is equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is not equal to " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_NOT_EQUAL_TO:
			if (!driver.getCurrentUrl().equals(paramValue)) {
				System.out.println("Title is not equal to " + paramValue);
				return true;
			} else {
				System.out.println("Title is equal to " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_CONTAINS:
			if (driver.getCurrentUrl().contains(paramValue)) {
				System.out.println("Title contains " + paramValue);
				return true;
			} else {
				System.out.println("Title does not contain " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH:
			if (driver.getCurrentUrl().startsWith(paramValue)) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_STARTS_WITH_IGNORING_CASE:
			if (driver.getCurrentUrl().toLowerCase().startsWith(paramValue.toLowerCase())) {
				System.out.println("Title starts with " + paramValue);
				return true;
			} else {
				System.out.println("Title does not start with " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN:
			if (!driver.getCurrentUrl().contains(paramValue)) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return driver.getCurrentUrl();
			}

			// break;
		case TappValidations.STRING_DOES_NOT_CONTAIN_IGNORING_CASE:
			if (!driver.getCurrentUrl().toLowerCase().contains(paramValue.toLowerCase())) {
				System.out.println("Title does not contain " + paramValue);
				return true;
			} else {
				System.out.println("Title contains " + paramValue);
				return driver.getCurrentUrl();
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object From(WebDriver driver, boolean isAlert, String parameter, int operation_id) {
		// TODO Auto-generated method stub

		switch (operation_id) {
		case TappOperations.SELECT_POSITIVE:
			try {
				driver.switchTo().alert().accept();
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.SELECT_NEGATIVE:
			try {
				driver.switchTo().alert().dismiss();
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		case TappOperations.SELECT_POSITIVE_WITH_TEXT:
			try {
				paramValue = (String) utilities.getFunctionResult(valuesMap, parameter, "parameter");
			} catch (NullPointerException e) {

			}
			try {
				driver.switchTo().alert().sendKeys(paramValue);
				driver.switchTo().alert().accept();
				return true;
			} catch (Exception e) {
				return false;
			}
			// break;
		default:
			return false;
		}
	}

	public static final Object Switch(WebElement webelement) {
		// TODO Auto-generated method stub
		try {
			Click(webelement);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToWebViewWithURL(String base_url, WebDriver driver, String object) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
		} catch (NullPointerException e) {

		}
		try {
			if (paramValue.startsWith("http")) {
				driver.get(paramValue.trim());
			} else {
				driver.get(base_url + paramValue.trim());
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SubmitForm(WebElement webelement) {
		// TODO Auto-generated method stub
		try {
			webelement.submit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToIFrame(WebDriver webdriver, String object) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, object, "object");
		} catch (NullPointerException e) {

		}
		try {
			webdriver.switchTo().frame(paramValue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Object SwitchToDefaultWebContent(WebDriver webdriver) {
		// TODO Auto-generated method stub
//		try {
//			paramValue = (String) utilities.getFunctionResult(valuesMap, step.getObject().getObject_identifier(), "object");
//		} catch (NullPointerException e) {
//
//		}
		try {
			webdriver.switchTo().defaultContent();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object HoverTo(WebDriver driver, WebElement webelement) {
		// TODO Auto-generated method stub
		try {
			Actions action = new Actions(driver);
			action.moveToElement(webelement).build().perform();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object SwitchToChildWindow(WebDriver webdriver) {
		// TODO Auto-generated method stub

		int parentindex = 0;
		List<String> parentHandle = new ArrayList<String>();
		try {
			for (int x = 0; x < webdriver.getWindowHandles().stream().count() - 1; x++) {
				parentHandle.clear();
				parentHandle.add(webdriver.getWindowHandles().stream().toArray()[x].toString());
			}
			webdriver.getWindowHandles().stream().forEach((winHandle) -> {
				webdriver.switchTo().window(winHandle);
			});
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public static final Object SwitchToParentWindow(WebDriver webdriver) {
		// TODO Auto-generated method stub
		List<String> parentHandle = new ArrayList<String>();
		try {
			for (int x = 0; x < webdriver.getWindowHandles().stream().count() - 1; x++) {
				parentHandle.clear();
				parentHandle.add(webdriver.getWindowHandles().stream().toArray()[x].toString());
			}

			try {
				if ((webdriver.getWindowHandles().stream().count() - 1) > 0) {
					webdriver.close();
					webdriver.switchTo().window(parentHandle.get(0));
				} else {
					webdriver.switchTo().window(webdriver.getWindowHandles().stream().toArray()[0].toString());
				}
			} catch (Exception e) {
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object EnterTable(WebElement webelement, String parameter) {
		// TODO Auto-generated method stub
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, parameter, "parameter");
		} catch (NullPointerException e) {

		}

		isWithinTable = true;
		rows_table = webelement.findElements(By.tagName("tr"));
		int column, row = 0;

		try {
			for (row = 0; row < rows_table.size(); row++) {
				columns_table = rows_table.get(row).findElements(By.tagName("td"));
				for (column = 0; column < columns_table.size(); column++) {
					if (columns_table.get(column).getText().toLowerCase().contains(paramValue.toLowerCase())) {
						isFound = true;
						selectedrow = row;
					}
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static final Object Wait(int seconds) {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(seconds);
			return true;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public static final Object ValidateCSS(WebElement webelement, String parameter, String validation_data) {
		// TODO Auto-generated method stub
		String texttotest;
		try {
			paramValue = (String) utilities.getFunctionResult(valuesMap, validation_data, "data");
		} catch (NullPointerException e) {

		}

		if (parameter.contains("color")) {
			if (paramValue.startsWith("#")) {

				String dervcol = webelement.getCssValue(parameter);
				System.out.println(dervcol);
				dervcol = dervcol.replace("(", "");
				System.out.println(dervcol);
				dervcol = dervcol.replace(")", "");
				System.out.println(dervcol);

				if (dervcol.startsWith("rgba"))
					dervcol = dervcol.replace("rgba", "");

				if (dervcol.startsWith("rgb"))
					dervcol = dervcol.replace("rgb", "");

				System.out.println(dervcol);

				String[] derivedcolor = dervcol.split(", ");
				int r = Integer.parseInt(derivedcolor[0].trim());
				int g = Integer.parseInt(derivedcolor[1].trim());
				int b = Integer.parseInt(derivedcolor[2].trim());
				texttotest = "#" + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b);
			} else {
				texttotest = webelement.getCssValue(parameter);
			}
		} else {
			texttotest = webelement.getCssValue(parameter);
		}

		if (texttotest.equalsIgnoreCase(paramValue)) {
			return true;
		} else {
			return texttotest;
		}

	}
}
