package cup.automation.tapp;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class TAppAutomationObjects {

	private WebDriver webDriver;
	private WebElement webElement;
	private List<WebElement> webElements;

	private AndroidDriver androidDriver;
	private AndroidElement androidElement;
	private List<AndroidElement> androidElements;
	private IOSDriver iosDriver;
	private IOSElement iosElement;
	private List<IOSElement> iosElements;
	
	public TAppAutomationObjects() {

	}
	
	/**
	 * @return the webdriver
	 */
	public WebDriver getWebdriver() {
		return webDriver;
	}

	/**
	 * @param webdriver
	 *            the webdriver to set
	 */
	public void setWebdriver(WebDriver webdriver) {
		this.webDriver = webdriver;
	}

	/**
	 * @return the webelement
	 */
	public WebElement getWebelement() {
		return webElement;
	}

	/**
	 * @param webelement
	 *            the webelement to set
	 */
	public void setWebelement(WebElement webelement) {
		this.webElement = webelement;
	}

	/**
	 * @return the webelements
	 */
	public List<WebElement> getWebelements() {
		return webElements;
	}

	/**
	 * @param webelements
	 *            the webelements to set
	 */
	public void setWebelements(List<WebElement> webelements) {
		this.webElements = webelements;
	}

	/**
	 * @return the androiddriver
	 */
	public AndroidDriver getAndroiddriver() {
		return androidDriver;
	}

	/**
	 * @param androiddriver
	 *            the androiddriver to set
	 */
	public void setAndroiddriver(AndroidDriver androiddriver) {
		this.androidDriver = androiddriver;
	}

	/**
	 * @return the iosdriver
	 */
	public IOSDriver getIosdriver() {
		return iosDriver;
	}

	/**
	 * @param iosdriver
	 *            the iosdriver to set
	 */
	public void setIosdriver(IOSDriver iosdriver) {
		this.iosDriver = iosdriver;
	}

	/**
	 * @return the androidelement
	 */
	public AndroidElement getAndroidelement() {
		return androidElement;
	}

	/**
	 * @param androidelement
	 *            the androidelement to set
	 */
	public void setAndroidelement(AndroidElement androidelement) {
		this.androidElement = androidelement;
	}

	/**
	 * @return the androidelements
	 */
	public List<AndroidElement> getAndroidelements() {
		return androidElements;
	}

	/**
	 * @param androidelements
	 *            the androidelements to set
	 */
	public void setAndroidelements(List<AndroidElement> androidelements) {
		this.androidElements = androidelements;
	}

	/**
	 * @return the ioselement
	 */
	public IOSElement getIoselement() {
		return iosElement;
	}

	/**
	 * @param ioselement
	 *            the ioselement to set
	 */
	public void setIoselement(IOSElement ioselement) {
		this.iosElement = ioselement;
	}

	/**
	 * @return the ioselements
	 */
	public List<IOSElement> getIoselements() {
		return iosElements;
	}

	/**
	 * @param ioselements
	 *            the ioselements to set
	 */
	public void setIoselements(List<IOSElement> ioselements) {
		this.iosElements = ioselements;
	}
	
	public void showBrowser(String applocation) {
		getWebdriver().get(applocation);
	}
	
	public String identifyWebElement(String locator) {
		// TODO Auto-generated method stub
		WebElement element = null;
		try {
			element = getWebdriver().findElement(By.id(locator.trim()));
			setWebelement(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.name(locator.trim()));
				setWebelement(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.tagName(locator.trim()));
				setWebelement(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.linkText(locator.trim()));
				setWebelement(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.partialLinkText(locator.trim()));
				setWebelement(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.className(locator.trim()));
				setWebelement(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.xpath(locator.trim()));
				setWebelement(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElement(By.cssSelector(locator.trim()));
				setWebelement(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tapp.cup.team.automation.interfaces.TappSetup#identifyWebElements(java.
	 * lang.String)
	 */
	public String identifyWebElements(String locator) {
		// TODO Auto-generated method stub
		List<WebElement> element = null;
		try {
			element = getWebdriver().findElements(By.id(locator.trim()));
			setWebelements(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.name(locator.trim()));
				setWebelements(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.tagName(locator.trim()));
				setWebelements(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.linkText(locator.trim()));
				setWebelements(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.partialLinkText(locator.trim()));
				setWebelements(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.className(locator.trim()));
				setWebelements(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.xpath(locator.trim()));
				setWebelements(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = getWebdriver().findElements(By.cssSelector(locator.trim()));
				setWebelements(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tapp.cup.team.automation.interfaces.TappSetup#identifyAndroidElement(java
	 * .lang.String)
	 */
	public String identifyAndroidElement(String locator) {
		// TODO Auto-generated method stub
		AndroidElement element = null;
		try {
			element = (AndroidElement) getAndroiddriver().findElement(By.id(locator.trim()));
			setAndroidelement(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.name(locator.trim()));
				setAndroidelement(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.tagName(locator.trim()));
				setAndroidelement(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.linkText(locator.trim()));
				setAndroidelement(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.partialLinkText(locator.trim()));
				setAndroidelement(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.className(locator.trim()));
				setAndroidelement(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.xpath(locator.trim()));
				setAndroidelement(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (AndroidElement) getAndroiddriver().findElement(By.cssSelector(locator.trim()));
				setAndroidelement(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tapp.cup.team.automation.interfaces.TappSetup#identifyAndroidElements(
	 * java.lang.String)
	 */

	public String identifyAndroidElements(String locator) {
		// TODO Auto-generated method stub
		List<AndroidElement> element = null;
		try {
			element = (List<AndroidElement>) getAndroiddriver().findElements(By.id(locator.trim()));
			setAndroidelements(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.name(locator.trim()));
				setAndroidelements(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.tagName(locator.trim()));
				setAndroidelements(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.linkText(locator.trim()));
				setAndroidelements(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.partialLinkText(locator.trim()));
				setAndroidelements(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.className(locator.trim()));
				setAndroidelements(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.xpath(locator.trim()));
				setAndroidelements(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<AndroidElement>) getAndroiddriver().findElements(By.cssSelector(locator.trim()));
				setAndroidelements(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tapp.cup.team.automation.interfaces.TappSetup#identifyIOSElement(java.
	 * lang.String)
	 */
	public String identifyIOSElement(String locator) {
		IOSElement element = null;
		try {
			element = (IOSElement) getIosdriver().findElement(By.id(locator.trim()));
			setIoselement(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.name(locator.trim()));
				setIoselement(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.tagName(locator.trim()));
				setIoselement(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.linkText(locator.trim()));
				setIoselement(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.partialLinkText(locator.trim()));
				setIoselement(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.className(locator.trim()));
				setIoselement(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.xpath(locator.trim()));
				setIoselement(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (IOSElement) getIosdriver().findElement(By.cssSelector(locator.trim()));
				setIoselement(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * tapp.cup.team.automation.interfaces.TappSetup#identifyIOSElements(java.
	 * lang.String)
	 */
	public String identifyIOSElements(String locator) {
		// TODO Auto-generated method stub
		List<IOSElement> element = null;
		try {
			element = (List<IOSElement>) getIosdriver().findElements(By.id(locator.trim()));
			setIoselements(element);

			return "id";
		} catch (Exception e) {

		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.name(locator.trim()));
				setIoselements(element);

				return "name";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.tagName(locator.trim()));
				setIoselements(element);

				return "tagName";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.linkText(locator.trim()));
				setIoselements(element);

				return "linkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.partialLinkText(locator.trim()));
				setIoselements(element);

				return "partialLinkText";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.className(locator.trim()));
				setIoselements(element);

				return "className";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.xpath(locator.trim()));
				setIoselements(element);

				return "xpath";
			} catch (Exception e) {

			}
		}

		if (element == null) {
			try {
				element = (List<IOSElement>) getIosdriver().findElements(By.cssSelector(locator.trim()));
				setIoselements(element);
				return "cssSelector";
			} catch (Exception e) {

			}
		}

		return null;
	}
	
	public File getWebScreenShot() {
		File scrFile = ((TakesScreenshot) getWebdriver()).getScreenshotAs(OutputType.FILE);
		return scrFile;
	}

	public File getAndroidScreenShot(int runhistory_id, int teststepresult_id) {
		File scrFile = getAndroiddriver().getScreenshotAs(OutputType.FILE);
		return scrFile;
	}

	public File getIosScreenShot(int runhistory_id, int teststepresult_id) {
		File scrFile = getIosdriver().getScreenshotAs(OutputType.FILE);
		return scrFile;
	}
	
	
}
