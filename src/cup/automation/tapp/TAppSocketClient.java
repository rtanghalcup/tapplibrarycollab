/**
 * 
 */
package cup.automation.tapp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author abernal
 */
public class TAppSocketClient {

    String server_address;
    static int port;
    Socket socket;
    public TAppSocketClient(String server_address, int port) {
        this.server_address = server_address;
        this.port = port;
    }
    
    public Socket getClient(){
        try {
            return new Socket(this.server_address, this.port);
        } catch (Exception ex) {
            Logger.getLogger(TAppSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void sendRequest(String request){
        PrintWriter out = null;
        try {
            out = new PrintWriter(this.socket.getOutputStream(), true);
            out.println(request);
            System.out.println(request);
        } catch (Exception ex) {
            Logger.getLogger(TAppSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public String getResponse(){
        BufferedReader input = null;
        try {
            input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            return input.readLine();
        } catch (Exception ex) {
            Logger.getLogger(TAppSocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return null;
    }
    
    public void connectToServer() {
//        while (true) {
//            try {
                this.socket = getClient();
//                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
//                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//                BufferedReader k = new BufferedReader(new InputStreamReader(System.in));
//                String answer = input.readLine();
//                try {
//                    if (!answer.equals("")) {
//                        JOptionPane.showMessageDialog(null, answer);
//                    }
//                } catch (NullPointerException e) {
//                    System.out.printf(e.toString());
//                }
//                out.println(k.readLine());
//        System.exit(0);
//            } catch (Exception ex) {
//                Logger.getLogger(TAppSocketClient.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
        
        
    }
    
    public static void main(String args[]) {
        TAppSocketClient client = new TAppSocketClient("192.168.243.119", 9898);
        client.connectToServer();
        System.out.println(client.getResponse());
//        client.sendRequest("create_emulator 18 480 800 5560");
    }

}
