package cup.automation.tapp;

public class TAppServer_Settings {

	private
	int id;
	
	private
	String firefox_server;
	
	private
	int firefox_port;

	private
	String chrome_server;
	
	private
	int chrome_port;
	
	private
	String ie_server;

	private
	int ie_port;

	private
	String safari_server;
	
	private
	int safari_port;

	private
	String edge_server;
	
	private
	int edge_port;

	private
	String android_server = "127.0.0.1";
	
	private
	int android_port = 4723;

	private
	String ios_server;
	
	private
	int ios_port;
	
	private
	String report_location;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the firefox_server
	 */
	public String getFirefox_server() {
		return firefox_server;
	}

	/**
	 * @param firefox_server the firefox_server to set
	 */
	public void setFirefox_server(String firefox_server) {
		this.firefox_server = firefox_server;
	}

	/**
	 * @return the firefox_port
	 */
	public int getFirefox_port() {
		return firefox_port;
	}

	/**
	 * @param firefox_port the firefox_port to set
	 */
	public void setFirefox_port(int firefox_port) {
		this.firefox_port = firefox_port;
	}

	/**
	 * @return the chrome_server
	 */
	public String getChrome_server() {
		return chrome_server;
	}

	/**
	 * @param chrome_server the chrome_server to set
	 */
	public void setChrome_server(String chrome_server) {
		this.chrome_server = chrome_server;
	}

	/**
	 * @return the chrome_port
	 */
	public int getChrome_port() {
		return chrome_port;
	}

	/**
	 * @param chrome_port the chrome_port to set
	 */
	public void setChrome_port(int chrome_port) {
		this.chrome_port = chrome_port;
	}

	/**
	 * @return the ie_server
	 */
	public String getIe_server() {
		return ie_server;
	}

	/**
	 * @param ie_server the ie_server to set
	 */
	public void setIe_server(String ie_server) {
		this.ie_server = ie_server;
	}

	/**
	 * @return the ie_port
	 */
	public int getIe_port() {
		return ie_port;
	}

	/**
	 * @param ie_port the ie_port to set
	 */
	public void setIe_port(int ie_port) {
		this.ie_port = ie_port;
	}

	/**
	 * @return the safari_server
	 */
	public String getSafari_server() {
		return safari_server;
	}

	/**
	 * @param safari_server the safari_server to set
	 */
	public void setSafari_server(String safari_server) {
		this.safari_server = safari_server;
	}

	/**
	 * @return the safari_port
	 */
	public int getSafari_port() {
		return safari_port;
	}

	/**
	 * @param safari_port the safari_port to set
	 */
	public void setSafari_port(int safari_port) {
		this.safari_port = safari_port;
	}

	/**
	 * @return the edge_server
	 */
	public String getEdge_server() {
		return edge_server;
	}

	/**
	 * @param edge_server the edge_server to set
	 */
	public void setEdge_server(String edge_server) {
		this.edge_server = edge_server;
	}

	/**
	 * @return the edge_port
	 */
	public int getEdge_port() {
		return edge_port;
	}

	/**
	 * @param edge_port the edge_port to set
	 */
	public void setEdge_port(int edge_port) {
		this.edge_port = edge_port;
	}

	/**
	 * @return the android_server
	 */
	public String getAndroid_server() {
		return android_server;
	}

	/**
	 * @param android_server the android_server to set
	 */
	public void setAndroid_server(String android_server) {
		this.android_server = android_server;
	}

	/**
	 * @return the android_port
	 */
	public int getAndroid_port() {
		return android_port;
	}

	/**
	 * @param android_port the android_port to set
	 */
	public void setAndroid_port(int android_port) {
		this.android_port = android_port;
	}

	/**
	 * @return the ios_server
	 */
	public String getIos_server() {
		return ios_server;
	}

	/**
	 * @param ios_server the ios_server to set
	 */
	public void setIos_server(String ios_server) {
		this.ios_server = ios_server;
	}

	/**
	 * @return the ios_port
	 */
	public int getIos_port() {
		return ios_port;
	}

	/**
	 * @param ios_port the ios_port to set
	 */
	public void setIos_port(int ios_port) {
		this.ios_port = ios_port;
	}

	/**
	 * @return the report_location
	 */
	public String getReport_location() {
		return report_location;
	}

	/**
	 * @param report_location the report_location to set
	 */
	public void setReport_location(String report_location) {
		this.report_location = report_location;
	}
}
