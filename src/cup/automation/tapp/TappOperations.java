/**
 * 
 */
package cup.automation.tapp;

/**
 * @author abernal
 *
 */
public class TappOperations {
	public static final int SELECT_ITEM = 1;
	public static final int TYPE_TEXT = 2;
	public static final int SEARCH_FOR = 3;
	public static final int PERFORM_TRANSACTION_USING = 4;
	public static final int IF = 5;
	public static final int TO_ON = 6;
	public static final int TO_OFF = 7;
	public static final int RIGHT_TO_LEFT = 8;
	public static final int LEFT_TO_RIGHT = 9;
	public static final int SELECT_POSITIVE = 10;
	public static final int SELECT_NEGATIVE = 11;
	public static final int SELECT_POSITIVE_WITH_TEXT = 12;
	public static final int GET_ROW_NO_OF = 13;
	public static final int STORE_VALUE_TO_MAP = 14;
	public static final int TOP_TO_BOTTOM = 15;
	public static final int BOTTOM_TO_TOP = 16;
	public static final int TO_PORTRAIT = 17;
	public static final int TO_LANDSCAPE = 18;
	public static final int AUTHENTICATE_USING = 19;
}
