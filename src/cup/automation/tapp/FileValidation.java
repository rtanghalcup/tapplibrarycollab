/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cup.automation.tapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.opencsv.CSVReader;

/**
 *
 * @author abernal
 */
public class FileValidation {
    FileInputStream fis;
    FileReader reader;
    private boolean isExcel = false;
    private boolean isXML = false;
    Document doc = null;
    String xmlcontent = "";
    
    public static void main(String[] args) {
        FileValidation mt = new FileValidation("d:/docs/excel/time.xlsx");
        try {
            System.out.println(mt.displayExcelCellContent(1,1));
        } catch (NullPointerException e) {

        }
        try {
//            mt.displayGenericContent();
            System.out.println(mt.displayGenericCellContent(2, 2));
        } catch (NullPointerException e) {

        }
    }
    
    public FileValidation(String filename) {
//        File file = new File("d:/docs/excel/time.xlsx");
//        File file = new File("d:/docs/excel/ANZGO Regression Scripts Development.xlsx");
        File file = new File(filename);
        if (file.getAbsolutePath().endsWith(".xlsx")) {
            initExcel(file);
            isExcel(true);
        } else {
            initCSV(file);
            isExcel(false);
        }

    }
    
     public boolean isExcel(){
        return isExcel;
    }
    
    private void isExcel(boolean isExcel){
        this.isExcel = isExcel;
    }

    /**
	 * @return the isXML
	 */
	public boolean isXML() {
		return isXML;
	}

	/**
	 * @param isXML the isXML to set
	 */
	public void setXML(boolean isXML) {
		this.isXML = isXML;
	}

	public void closeGenericFile() {
        try {
            this.reader.close();
        } catch (IOException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeExcelFile() {
        try {
            this.fis.close();
        } catch (IOException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initCSV(File file) {
        try {
            this.reader = new FileReader(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initExcel(File file) {
        try {
            this.fis = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initXML(File file) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            dBuilder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId)
                        throws SAXException, IOException {
                    if (systemId.contains(".dtd")) {
                        return new InputSource(new StringReader(""));
                    } else {
                        return null;
                    }
                }
            });
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            this.doc = dBuilder.parse(file);            
        } catch (SAXException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String displayGenericCellContent(int column, int row) {
        CSVReader reader = new CSVReader(this.reader, ',', '\"');
        List<String[]> rows = new ArrayList<String[]>();

        try {
            rows = reader.readAll();
        } catch (IOException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
        }

        closeGenericFile();
        return rows.get(row - 1)[column - 1];

    }

    public String displayGenericContent() {
        CSVReader reader = new CSVReader(this.reader, ',', '\"');
        String[] nextLine;
        String output = "";
        try {
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                for (int x = 0; x < nextLine.length; x++) {
                    output += nextLine[x] + "-";
                }
                output += "\n";
            }
            closeGenericFile();
            return output;
        } catch (IOException ex) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }

    }

    public String displayExcelCellContent(int column, int row) {

        String content = "";
        try (XSSFWorkbook workbook = new XSSFWorkbook(this.fis)) {
            DataFormatter formatter = new DataFormatter();

            FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
            XSSFCell cell = workbook.getSheetAt(0).getRow(row-1).getCell(column-1);
            content = formatter.formatCellValue(cell, formulaEvaluator);
            closeExcelFile();
            return content;

        } catch (IOException e) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }

    }

    public String displayExcelContent() {

        try (HSSFWorkbook workbook = new HSSFWorkbook(this.fis)) {
            HSSFSheet firstSheet = workbook.getSheetAt(0);
            String output = "";
            for (Iterator iterator = firstSheet.rowIterator(); iterator.hasNext();) {
                HSSFRow row = (HSSFRow) iterator.next();

                row.setRowNum(1);
                for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                    HSSFCell cell = row.getCell(i);
                    DataFormatter formatter = new DataFormatter();
                    FormulaEvaluator formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();
                    output += formatter.formatCellValue(cell, formulaEvaluator);
                    output += "-";
                }
                output += "\n";
                closeExcelFile();
            }
            return output;
        } catch (IOException e) {
            Logger.getLogger(FileValidation.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
//        }

    }
    
    public String displayXMLContent() {
        xmlcontent = "";
        this.doc.getDocumentElement().normalize();
//        content = doc.getDocumentElement().getNodeName() + "\n";

//
        if (this.doc.hasChildNodes()) {

            readXML(this.doc.getChildNodes());

        }

        return xmlcontent;
    }

    private void readXML(NodeList nodeList) {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                // get node name and value
//                System.out.print("<" + tempNode.getNodeName());
                xmlcontent += "<" + tempNode.getNodeName();
                if (tempNode.hasAttributes()) {

                    // get attributes names and values
                    NamedNodeMap nodeMap = tempNode.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {

                        Node node = nodeMap.item(i);
//                        System.out.print(" " + node.getNodeName() + " = \"");
                        xmlcontent += node.getNodeName() + " = \"";
//                        System.out.print(node.getNodeValue() + "\">");
                        xmlcontent += node.getNodeValue() + "\">";

                    }

                } else {
//                    System.out.print(">");
                    xmlcontent += ">";
                }
//                if (tempNode.getNodeValue() != null) {
//                System.out.print(tempNode.getTextContent());
                xmlcontent += tempNode.getTextContent();
//                }
                if (tempNode.hasChildNodes()) {

                    // loop again if has child nodes
                    readXML(tempNode.getChildNodes());

                }

//                System.out.println("</" + tempNode.getNodeName() + ">");
                xmlcontent += "</" + tempNode.getNodeName() + ">\n";

            }

        }
//        return xmlcontent;
    }

    public String displayXMLNodeContent(String tag) {
        xmlcontent = "";
        this.doc.getDocumentElement().normalize();
//        content = doc.getDocumentElement().getNodeName() + "\n";
        
//
        if (this.doc.hasChildNodes()) {

            readXMLNode(tag, this.doc.getChildNodes());

        }

        return xmlcontent;
    }

    private void readXMLNode(String tag, NodeList nodeList) {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                // get node name and value
//                System.out.print("<" + tempNode.getNodeName());
                if (tempNode.getNodeName().equals(tag)) {
                    xmlcontent += "<" + tempNode.getNodeName();
                }
                if (tempNode.hasAttributes()) {

                    // get attributes names and values
                    NamedNodeMap nodeMap = tempNode.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {

                        Node node = nodeMap.item(i);
//                        System.out.print(" " + node.getNodeName() + " = \"");
//                        xmlcontent += node.getNodeName() + " = \"";
//                        System.out.print(node.getNodeValue() + "\">");
//                        xmlcontent += node.getNodeValue() + "\">";

                    }

                } else {
//                    System.out.print(">");
                    if (tempNode.getNodeName().equals(tag)) {
                        xmlcontent += ">";
                    }
                }
//                if (tempNode.getNodeValue() != null) {
//                System.out.print(tempNode.getTextContent());
                if (tempNode.getNodeName().equals(tag)) {
                    xmlcontent += tempNode.getTextContent();
                }
//                }
                if (tempNode.hasChildNodes()) {

                    // loop again if has child nodes
                    readXMLNode(tag,tempNode.getChildNodes());

                }

//                System.out.println("</" + tempNode.getNodeName() + ">");
                if (tempNode.getNodeName().equals(tag)) {
                    xmlcontent += "</" + tempNode.getNodeName() + ">\n";
                }

            }

        }
//        return xmlcontent;
    }
    
    
}
	