/**
 * 
 */
package cup.automation.tapp;

/**
 * @author abernal
 *
 */
public class TappValidations {	
	public static final int STRING_EQUALS = 1;
	public static final int STRING_EQUALS_IGNORE_CASE = 2;
	public static final int STRING_NOT_EQUAL_TO = 3;
	public static final int INTEGER_GREATER_THAN = 4;
	public static final int INTEGER_GREATER_THAN_OR_EQUAL_TO = 5;
	public static final int INTEGER_LESS_THAN = 6;
	public static final int INTEGER_LESS_THAN_OR_EQUAL_TO = 7;
	public static final int INTEGER_NOT_EQUAL_TO = 8;
	public static final int INTEGER_EQUAL_TO = 9;
	public static final int IN_ASCENDING_ORDER = 10;
	public static final int IN_DESCENDING_ORDER = 11;
	public static final int STRING_CONTAINS = 12;
	public static final int EXISTS = 13;
	public static final int NOT_EXISTS = 14;
	public static final int STRING_STARTS_WITH = 15;
	public static final int STRING_STARTS_WITH_IGNORING_CASE = 16;
	public static final int IS_SELECTED = 17;
	public static final int IS_NOT_SELECTED = 18;
	public static final int IN_PORTRAIT = 19;
	public static final int IN_LANDSCAPE = 20;
	public static final int STRING_DOES_NOT_CONTAIN = 21;
	public static final int STRING_DOES_NOT_CONTAIN_IGNORING_CASE = 22;
}
