package cup.automation.tapp.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cup.automation.tapp.TAppAutomationObjects;
import cup.automation.tapp.TAppServer_Settings;
import cup.automation.tapp.TAppSetup;
import cup.automation.tapp.TAppWebTest;
import cup.automation.tapp.TappOperations;

public class TAppTestCaseSample {

	TAppSetup setup;
	TAppAutomationObjects test;
	@Before
	public void setUp() throws Exception {
		TAppServer_Settings settings = new TAppServer_Settings();
		this.setup = new TAppSetup(settings);
		
		this.setup.setApplocation("http://www.google.com");
		
		this.test = new TAppAutomationObjects();
		this.test.setWebdriver(this.setup.useChrome());
		this.test.showBrowser(this.setup.getApplocation());
		
	}

	@After
	public void tearDown() throws Exception {
		this.test.getWebdriver().close();
	}

	@Test
	public void test() {
		
		this.test.identifyWebElement("lst-ib");
		TAppWebTest.From(this.test.getWebelement(), "automation", TappOperations.SEARCH_FOR);
		
		this.test.identifyWebElement("Automation - Wikipedia");
		TAppWebTest.Click(this.test.getWebelement());
		
		
	}

}
